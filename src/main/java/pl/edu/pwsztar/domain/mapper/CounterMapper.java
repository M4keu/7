package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CounterMoviesDTO;

@Component
public class CounterMapper {
    public CounterMoviesDTO mapToCounter(Long movie) {
        CounterMoviesDTO counterMoviesDTO = new CounterMoviesDTO();
         counterMoviesDTO.setCounter(movie);
        return counterMoviesDTO;
    }
}
