package pl.edu.pwsztar.domain.dto;

public class CounterMoviesDTO {
    private long counter;

    public CounterMoviesDTO() {
        this.counter = counter;
    };

    public void setCounter(long counter) {
        this.counter = counter;
    }

    public long countMovies() {
        return counter;
    }
}
