package pl.edu.pwsztar.domain.dto;

public class UpdateMovieDto {
    private String title;
    private String image;
    private int year;
    private String videoId;

    public UpdateMovieDto () {}

    public String getImage() {
        return image;
    }

    public int getYear() {
        return year;
    }

    public String getMovieId() {
        return videoId;
    }

    public String getTitle() {
        return title;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setMovieId(String movieId) {
        this.videoId = videoId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
